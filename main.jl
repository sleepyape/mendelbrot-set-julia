using MiniFB


"""
frame4zoom(1.2, 2,4, 100)

This function creates a new rectangle such that the point given by (x,y) will be centered 
and it will have the same propotions with other zoom values

"""
function frame4zoom(x::Real, y::Real, zoom::Real)

    if (zoom < 1.0)
        throw(AssertionError("Zoom parameter only can be greater or equal to 1.0"))
    end

    width = 3.5 / zoom
    height = 2.0 / zoom

    origin_x = x - (width / 2.0)
    origin_y = y + (height / 2.0)

    return (x = origin_x,
            y = origin_y,
            w = width,
            h = height)
end


"""
Main function, creates a mandelbrot zoom graphic using mini framebuffer library
"""
function mandelbrot()
    pallete = zeros(UInt32, 512)
    WIDTH = 600
    HEIGHT = 600
    inc = 90 / 64;
    for c in 1:64
        col = round(Int, ((255 * sin( (c-1) * inc * π / 180)) + 0.5));
        pallete[64*0 + c] = mfb_rgb(col,     0,       0);
        pallete[64*1 + c] = mfb_rgb(255,     col,     0);
        pallete[64*2 + c] = mfb_rgb(255-col, 255,     0);
        pallete[64*3 + c] = mfb_rgb(0,       255,     col);
        pallete[64*4 + c] = mfb_rgb(0,       255-col, 255);
        pallete[64*5 + c] = mfb_rgb(col,     0,       255);
        pallete[64*6 + c] = mfb_rgb(255,     0,       255-col);
        pallete[64*7 + c] = mfb_rgb(255-col, 0,       0);
    end
    window = mfb_open_ex("Mandelbrot Set", WIDTH, HEIGHT, MiniFB.WF_RESIZABLE);
    g_buffer = zeros(UInt32, WIDTH * HEIGHT)
    mfb_set_target_fps(120);

    max_iteration = 512

    zoom_center_real_comp = -0.7106
    zoom_center_img_comp = 0.2475

    zoom = 1.0

    while mfb_wait_sync(window)

        frame = frame4zoom(zoom_center_real_comp, zoom_center_img_comp, zoom);

        mandelbrot_x_min = frame.x;
        mandelbrot_x_max = frame.x + frame.w;
        mandelbrot_y_min = frame.y - frame.h;
        mandelbrot_y_max = frame.y;

        zoom *= 1.2;
        i = 1;

        for h in 1:HEIGHT
            for w in 1:WIDTH

                x_scaled = ((mandelbrot_x_max - mandelbrot_x_min) / WIDTH) * w + mandelbrot_x_min;
                y_scaled = ((mandelbrot_y_max - mandelbrot_y_min) / HEIGHT) * h + mandelbrot_y_min;
                
                real_part = 0.0;
                imaginary_part = 0.0;
            
                iteration = 1;

                while ((real_part * real_part + imaginary_part * imaginary_part <= 4) &&
                       iteration < max_iteration)
                
                    temp = real_part * real_part - imaginary_part * imaginary_part + x_scaled;
                    imaginary_part = 2 * real_part * imaginary_part + y_scaled;
                    real_part = temp;
                    iteration += 1;
                end

                g_buffer[i] = pallete[iteration];
                i += 1
            end
        end
        state = mfb_update(window, g_buffer);
        if state != MiniFB.STATE_OK
            break;
        end
    end
    mfb_close(window)
end

mandelbrot() 
