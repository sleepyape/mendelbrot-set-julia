#version 410

uniform vec2 centerPosition;
uniform vec2 screenResolution;
uniform float zoom;

out vec4 fragColour;
in vec4 gl_FragCoord;

void main() {

    float resw = screenResolution[0];
    float resh = screenResolution[1];

    float cx = centerPosition[0];
    float cy = centerPosition[1];

    float rw = 3.5f / zoom;
    float rh = 2.0f / zoom;

    float rx = cx - (rw/2.0f);
    float ry = cy + (rh/2.0f);

    float glx = 3.5f * (gl_FragCoord[0] / resw) - 2.0f;
    float gly = 2.0f * (gl_FragCoord[1] / resh) - 1.0f;

    float sx = (glx) * rw + rx;
    float sy = (gly) * rh + ry;

    float real = 0.0f;
    float img  = 0.0f;

    float current_iter = 0.0f;
    const float max_iter = 255.0f;

    while((real * real + img * img <= 4.0f) && current_iter <= max_iter )
    {
        float temp = real * real - img * img + sx;
        img = 2 * real * img + sy;
        real = temp;
        current_iter += 1.0f; 
    }

    fragColour = vec4(current_iter/255.0f, 0.416f, (255.0f - current_iter) / 255.0f, 1.0f);
}