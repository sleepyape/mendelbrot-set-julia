# So, I've watched a youtube video on Mandelbrot today

Title says it all, I just watched a video from the wonderful channel numberphile about the Mandelbrot sets (and julia sets for that matter) and wanted to write a simple program to see it myself.

Since I've been toying anyway, wanted to toy with Julia as well.

UPDATE: 

I was bored and added the functionality of zooming to a certain point, It is slow to calculate on CPU will add a version later that has the ability to 
run it on GPU

I also have a picture from it 😅

![img1](imgs/img1.png)

UPDATE 2:

I have a preliminary GPU implementation inside the shader_impl folder (entirely different project) It's based on the Opengl 4 tutorial works of [Gnimuc/Videre](https://github.com/Gnimuc/Videre) I really appraciated their? work, so please take a look at them.

Here is a screenshot for early gpu rendered version

![img2](imgs/img2.png)

## To Do (for GPU version)

- [ ] Fix Scale issues
- [ ] Unlock framerate
- [ ] Better colorscheme maybe ?

I will probably never going to do any of these tho 🤣🤣

# Running 

Use vscode julia extension, for gpu one, open shader.js and click the run button on top right, from the dropdown menu select run on REPL.

For the CPU one idea is the same, file to check is main.jl
